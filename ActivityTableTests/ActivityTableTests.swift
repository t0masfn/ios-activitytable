//
//  ActivityTableTests.swift
//  ActivityTableTests
//
//  Created by Tomás Fernández Velazco on 09/02/2021.
//

import XCTest
@testable import ActivityTable

class ActivityTableTests: XCTestCase {
        
    func testFormatterDate() {
    
        let date = "2016-05-23T00:00:00+00:00"
        
        let activityCell = ActivityTableCell()
        let passedDate = activityCell.dateCorrectShow(date: date)
        
        XCTAssertEqual("May 23 2016", passedDate)
    }
    
    func testUserReceived() {
        APIManager.getUserById(userId: "1") { (user, error) in
            if error == nil {
                XCTAssertEqual(user?.displayName, "Mikael")
            }
        }
    }
}
