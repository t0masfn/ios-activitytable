//
//  ActivityTableCell.swift
//  ActivityTable
//
//  Created by Tomás Fernández Velazco on 09/02/2021.
//

import UIKit
import Kingfisher

class ActivityTableCell: UITableViewCell {
  static let Identifier = "activityCell"
  
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var messageLbl: UILabel!
    @IBOutlet var amountLbl: UILabel!
    @IBOutlet var avatarImgView: UIImageView!
    
  func configureWithActivity(activity: Activity) {
    
    messageLbl.text = activity.message
    
    let attributed = messageLbl.attributedText?.replaceHTMLTag(tag: "strong", withAttributes: [
        NSAttributedString.Key.foregroundColor : UIColor.black
      ])
    
    messageLbl.attributedText = attributed
   
    //messageLbl.setHTMLFromString(htmlText: activity.message)
    dateLbl.text = dateCorrectShow(date: activity.timestamp)
    amountLbl.text = String(format: "$%.02f", activity.amount)
    getUserIdData(userId: "\(activity.userId)")
        
    amountLbl.textColor = UIColor.getGreenColor()
    
    messageLbl.bringSubviewToFront(amountLbl)
    avatarImgView.layer.cornerRadius = avatarImgView.frame.height / 2
    
    amountLbl.font = UIFont(name:"BentonSans-Regular",size:16)
    dateLbl.font = UIFont(name:"BentonSans-Regular",size:12)
  }
    
    
    func dateCorrectShow(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss+00:00"
        guard let date = dateFormatter.date(from: date) else {
            fatalError()
        }
        
        if Calendar.current.isDateInToday(date) {
            return "Today"
        }
        
        if Calendar.current.isDateInYesterday(date) {
            return "Yesterday"
        }
        
        dateFormatter.dateFormat = "d MMM yyyy"
        let newDate = dateFormatter.string(from: date)
        return newDate
    }
    
    func getUserIdData(userId: String) {
        APIManager.getUserById(userId: userId) { [self] (user, error) in
            if error == nil {
                updateAvatarImage(url: user!.avatarUrl)
            }
        }
    }
    
    func updateAvatarImage(url: String) {
        DispatchQueue.main.async { [self] in
            avatarImgView.kf.setImage(with: URL(string: url))
        }
    }
}
