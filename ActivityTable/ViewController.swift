//
//  ViewController.swift
//  ActivityTable
//
//  Created by Tomás Fernández Velazco on 09/02/2021.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher

class ViewController: UIViewController, UITableViewDelegate {
    
    let disposeBag = DisposeBag()
    var activities: BehaviorRelay<[Activity]> = BehaviorRelay(value: [])
    
    var weekDistanceValue = -2
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Activity"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupTableView()
        bindActivities()
        setNavBar()
    }
    
    func setNavBar() {
        //navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.layoutIfNeeded()
        UINavigationBar.appearance().isTranslucent = false
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "BentonSans-Medium", size: 16)!]
    }
    
    func setupTableView() {
        tableView.register(UINib.init(nibName: "ActivityCell", bundle: nil), forCellReuseIdentifier: "activityCell")
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 70
        tableView.separatorColor = UIColor.getGrayColor()
    }
    
    func bindActivities() {
    
        activities.asObservable()
            .bind(to: tableView.rx.items) { (tableView, row, element) in
                let cell = tableView.dequeueReusableCell(withIdentifier: "activityCell") as! ActivityTableCell
                cell.configureWithActivity(activity: element)
                return cell

            }
            .disposed(by: disposeBag)
        
        tableView.rx.modelSelected(Activity.self)
            .map{ URL(string: $0.message) }
            .subscribe(onNext: { url in
                                
                guard url != nil else {
                    return
                }
        }).disposed(by: disposeBag)
        
        fetchActivityData()
    }
    
    
    func fetchActivityData() {
        //Prepare dates with correct format to communicate with endpoint:
        
        let todayNow = Date().string(format: "yyyy-MM-dd'T'HH:mm:ss+00:00")
        let dateFrom = Calendar.current.date( byAdding: .weekOfMonth, value: weekDistanceValue, to: Date())!.string(format: "yyyy-MM-dd'T'HH:mm:ss+00:00")
        
        APIManager.getActivites(dateFrom: dateFrom, dateTo: todayNow) { [self] (result, error) in
            if let acts = result?.activities {
                self.activities.accept(acts)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //When tableView is on the bottom we add 2 weeks more of range and  bring from the API new activities based on that parameter until we get all the activities
        if indexPath.row + 1 == activities.value.count {
            weekDistanceValue = weekDistanceValue - 2
            fetchActivityData()
        }
    }
}

