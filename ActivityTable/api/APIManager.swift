//
//  APIManager.swift
//  ActivityTable
//
//  Created by Tomás Fernández Velazco on 09/02/2021.
//

import UIKit

typealias getActivitiesBlock = (Result?, Error?) -> Void
typealias getUserBlock = (User?, Error?) -> Void

class APIManager: NSObject {

    let baseURL = "http://qapital-ios-testtask.herokuapp.com"
    static let sharedInstance = APIManager()
    
    class func getActivites(dateFrom: String, dateTo: String, completionBlock: @escaping getActivitiesBlock) {
                
        if let url = URL(string: "http://qapital-ios-testtask.herokuapp.com/activities?from=\(dateFrom)&to=\(dateTo)") {
           URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data {
               do {
                 let result = try JSONDecoder().decode(Result.self, from: data)
                   
                   completionBlock(result, nil)
               } catch let error {
                 print(error)
                   completionBlock(nil, error)
               }
            }
          }.resume()
        }
    }
    
    class func getUserById(userId: String, completionBlock: @escaping getUserBlock) {
                
        if let url = URL(string: "http://qapital-ios-testtask.herokuapp.com/users/\(userId)") {
           URLSession.shared.dataTask(with: url) { data, response, error in
             if let data = data {
                do {
                  let user = try JSONDecoder().decode(User.self, from: data)
                    
                    completionBlock(user, nil)
                } catch let error {
                  print(error)
                    completionBlock(nil, error)
                }
             }
           }.resume()
        }
    }
}
