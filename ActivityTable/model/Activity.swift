//
//  Activity.swift
//  ActivityTable
//
//  Created by Tomás Fernández Velazco on 09/02/2021.
//

import Foundation

struct Activity: Decodable, Equatable {
  let message: String
  let amount: Double
  let userId: Int
  let timestamp: String
}

struct Result: Decodable {
  let activities: [Activity]
}



