//
//  User.swift
//  ActivityTable
//
//  Created by Tomás Fernández Velazco on 09/02/2021.
//

import Foundation

struct User: Decodable {
  let userId: Int
  let displayName: String
  let avatarUrl: String
}


