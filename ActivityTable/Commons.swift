//
//  Commons.swift
//  ActivityTable
//
//  Created by Tomás Fernández Velazco on 09/02/2021.
//

import UIKit

extension UILabel {
    //For the correct display of <strong></strong> in message attribute
    
    func setHTMLFromString(htmlText: String) {
        let modifiedFont = String(format:"<span style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: \(self.font!.pointSize)\">%@</span>", htmlText)

        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
        self.attributedText = attrStr
    }
}



extension UIImage {
    //For the correct size display of the user avatar
  func resizeImage(targetSize: CGSize) -> UIImage {
    let size = self.size
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    let newSize = widthRatio > heightRatio ?  CGSize(width: size.width * heightRatio, height: size.height * heightRatio) : CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    self.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    return newImage!
  }
}

extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}


extension String {
 func date(format: String) -> Date? {
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
         let date = dateFormatter.date(from: self)
         return date
     }
 }


//AppColors
extension UIColor{
    class func getGrayColor() -> UIColor{
        return UIColor(red: 0.89, green: 0.89, blue: 0.89, alpha: 1.00)
    }
    class func getGreenColor() -> UIColor{
        return UIColor(red: 0.35, green: 0.75, blue: 0.04, alpha: 1.00)
    }
}


extension NSAttributedString {
    func replaceHTMLTag(tag: String, withAttributes attributes: [NSAttributedString.Key : Any]?) -> NSAttributedString {
        let openTag = "<\(tag)>"
        let closeTag = "</\(tag)>"

        let resultingText: NSMutableAttributedString = self.mutableCopy() as! NSMutableAttributedString
        while true {
            let plainString = resultingText.string as NSString
            let openTagRange = plainString.range(of: openTag)
            if openTagRange.length == 0 {
                break
            }

            let affectedLocation = openTagRange.location + openTagRange.length

            let searchRange = NSRange(location: affectedLocation, length: plainString.length - affectedLocation)

            let closeTagRange = plainString.range(of: closeTag, options: NSString.CompareOptions.init(rawValue: 0), range: searchRange)
            
        
            resultingText.setAttributes(attributes, range: NSRange(location: affectedLocation, length: closeTagRange.location - affectedLocation))
            resultingText.deleteCharacters(in: closeTagRange)
            resultingText.deleteCharacters(in: openTagRange)
        }
        
        
        return resultingText as NSAttributedString
    }
}
let imageCache = NSCache<NSString, AnyObject>()

extension UIImage {
    static func loadImageUsingCacheWithUrlString(_ urlString: String, completion: @escaping (UIImage) -> Void) {
        if let cachedImage = imageCache.object(forKey: urlString as NSString) as? UIImage {
            completion(cachedImage)
        }

        //No cache, so create new one and set image
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if let error = error {
                print(error)
                return
            }

            DispatchQueue.main.async(execute: {
                if let downloadedImage = UIImage(data: data!) {
                    imageCache.setObject(downloadedImage, forKey: urlString as NSString)
                    completion(downloadedImage)
                }
            })

        }).resume()
    }
}
