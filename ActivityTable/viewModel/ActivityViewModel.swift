//
//  ActivityViewModel.swift
//  ActivityTable
//
//  Created by Tomás Fernández Velazco on 09/02/2021.
//

import Foundation
import RxSwift
import RxCocoa

//
//class ActivityViewModel {
//    
//    let items = PublishSubject<[Activity]>()
//    
//    func fetchActivityList(completionBlock: @escaping GetActivitiesCompletionBlock) {
//        let fromDate = "2016-05-23T00:00:00+00:00"
//        let toDate = "2021-05-23T00:00:00+00:00"
//        
//        if let url = URL(string: "http://qapital-ios-testtask.herokuapp.com/activities?from=\(fromDate)&to=<\(toDate)>") {
//           URLSession.shared.dataTask(with: url) { data, response, error in
//             if let data = data {
//                do {
//                  let activities = try JSONDecoder().decode([Activity].self, from: data)
//                    completionBlock(activities)
//                } catch let error {
//                  print(error)
//                }
//             }
//           }.resume()
//        }
//        
//        //items.onNext(ite)
//        items.onCompleted()
//    }
//    
//}
